# Lua Filters for Pandoc

This project provides [Lua Filters](https://pandoc.org/lua-filters.html)
adding the support of several features to [Pandoc](https://pandoc.org) so as to
enhance the documents produced from Markdown files.

## Requirements

You need at least **Pandoc 2.0** to apply Lua Filters.
No Lua interpreter is needed, as one is already bundled inside Pandoc.

## Using a Filter

To apply a Lua Filter, specify its path in the command line executing Pandoc,
as in the following example.

```bash
pandoc --lua-filter path/to/filter.lua -o example.pdf example.md
```

More informations about the use of Lua Filters can be read in the
[official Pandoc's documentation](https://pandoc.org/lua-filters.html).

## Available Filters

This section describes the various Lua Filters we provide.

### Support for GitLab Math

GitLab is able to render LaTeX equations in Markdown files, as described in
[its documentation](https://docs.gitlab.com/ee/user/markdown.html#math).
However, its syntax is not compatible with Pandoc.

The filter [`gitlab-math.lua`](gitlab-math.lua) translates GitLab's syntax so
as to make it compatible with Pandoc.

This filter can be used for **any kind** of output file.

### Opening External Links in New Tabs

A best practice when designing a website is to open links pointing to external
websites in new tabs.
However, this cannot be specified in Markdown, unless you use HTML's syntax.

The filter [`target-blank.lua`](target-blank.lua) parses each link of your
Markdown source file to detect whether its URL starts with either `http://` or
`https://` and, when it is the case, specifies in the corresponding `a` element
the attribute `target="_blank"` so as to open the link in a new tab.

This filter can only be used when converting Markdown to **HTML**.

### Writing Theorems with Block Quotes

In Markdown, there is no dedicated way to write theorems as in LaTeX.

The filter [`theorems.lua`](theorems.lua) provides a nice way to write
theorems using Markdown's *block quotes*.
If you want to write a theorem of type `theorem`, simply add `:theorem:` at the
beginning of the corresponding block quote.
If you want your theorem to have a title, write it right after the `:theorem:`
marker, separated by a single space.
The rest of the block quote will be interpreted as the content of the theorem.
You may of course use Markdown formatting in both the title and the content of
the theorem.

This filter can only be used when converting from Markdown to **LaTeX**.
You will also need to define the different theorems in the preamble
of the LaTeX document, e.g., using the `header-includes` variable.

Note that, currently, this filter is incompatible with the filter
[`beamerblocks.lua`](beamerblocks.lua).


### Writing Beamer Blocks with Block Quotes

In Markdown, there is no dedicated way to write Beamer blocks as in LaTeX.

The filter [`beamerblocks.lua`](beamerblocks.lua) provides a nice way to write
Beamer blocks using Markdown's *block quotes*.
If you want to write a block of type `exampleblock`, for instance, simply add
`:exampleblock:` at the beginning of the corresponding block quote.
If you want your block to have a title, write it right after the `:exampleblock:`
marker, separated by a single space.
The rest of the block quote will be interpreted as the content of the block.
You may of course use Markdown formatting in both the title and the content of
the theorem.

This filter can only be used when converting from Markdown to **LaTeX**.

Note that, currently, this filter is incompatible with the filter
[`theorems.lua`](theorems.lua).

### LaTeX Specific Environment

Some specific LaTeX constructs are not supported by Markdown.

The following filters allow a better integration of such constructs in
Markdown:

+ [`algorithm.lua`](algorithm.lua) translates any `algorithm` code block
  into the corresponding `algorithm` environment from the `algorithm2e`
  LaTeX package.

+ [`plantuml.lua`](plantuml.lua) translates any `plantuml` code block
  into the corresponding `plantuml` environment from the `plantuml`
  LaTeX package (note that this syntax is also that recognized by
  GitLab).

+ [`prooftree.lua`](prooftree.lua) translates any `prooftree` code block
  into the corresponding `prooftree` environment from the `bussproofs`
  LaTeX package.

+ [`tikz.lua`](tikz.lua) translates any `tikz` code block into the
  corresponding `tikzpicture` environment from the `tikz`
  LaTeX package.

These filters can only be used when converting from Markdown to **LaTeX**.
You will also need to include the appropriate packages in the preamble
of the LaTeX document, e.g., using the `header-includes` variable.

### Raw Code Blocks

Markdown is pretty cool, but it does not provide all features offered by LaTeX.
When one of these features is needed, you have to write LaTeX in your
Markdown source file, which will then be interpreted as such by Pandoc.
However, this LaTeX code has generally a poor rendering in Markdown viewers, or
on GitHub or GitLab.

The filter [`raw-latex.lua`](raw-latex.lua) allows to write LaTeX code that has
to be interpreted by Pandoc inside a code block marked as `raw`, so that it
appears as code in Markdown viewers, while it still has the expected rendering
once the document is built.

This filter can be used for **any kind** of output file.
