-- Lua Filter converting block quotes into Beamer blocks.
-- Copyright (c) 2021 - Romain WALLON, released under MIT license.

function BlockQuote(block)
    -- Looking for the name of the block.
    thm = block.content[1].c[1].c

    if (thm ~= nil) and (thm:sub(1, 1) == ":") and (thm:sub(-1) == ":")
    then
        -- Extracting the name of the block.
        name = thm:sub(2, #thm - 1)
        table.remove(block.content[1].c, 1)

        -- Removing the initial space, if any.
        if #block.content[1].c > 0
        then
            table.remove(block.content[1].c, 1)
        end

        -- Building the block.
        return {
            pandoc.Para {
                pandoc.RawInline("latex", "\\begin{" .. name .. "}{"),
                pandoc.Span(table.remove(block.content, 1).c),
                pandoc.RawInline("latex", "}")
            },
            pandoc.Div(block.content),
            pandoc.RawBlock("latex", "\\end{" .. name .. "}")
        }
    end
end

