-- Lua Filter opening external links in new tabs.
-- Copyright (c) 2019 - Romain WALLON, released under MIT license.

function Link(a)
    if (a.target:sub(1, 7) == "http://") or (a.target:sub(1, 8) == "https://")
    then
        -- This is an external link: it has to be opened in a new tab.
        return pandoc.Link(a.content, a.target, a.title, pandoc.Attr("", {}, {{ "target", "_blank" }}))
    end

    -- This is an internal link: it is left as is.
    return a
end
