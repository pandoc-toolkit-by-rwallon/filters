-- Lua Filter allowing "plantuml" code blocks to be interpreted in LaTeX
-- thanks to the "plantuml" LaTeX package.
-- Copyright (c) 2020 - Romain WALLON, released under MIT license.

function CodeBlock(block)
    if block.classes[1] == "plantuml"
    then
        -- Converting this code block to a plantuml LaTeX environment.
        return {
            pandoc.RawBlock("latex", "\\begin{plantuml}"),
            pandoc.RawBlock("latex", block.text),
            pandoc.RawBlock("latex", "\\end{plantuml}")
        }
    end

    -- This code block is left as is.
    return block
end
