-- Lua Filter allowing "prooftree" code blocks to be interpreted in LaTeX
-- thanks to the "bussproofs" LaTeX package.
-- Copyright (c) 2020 - Romain WALLON, released under MIT license.

function CodeBlock(block)
    if block.classes[1] == "prooftree"
    then
        -- Converting this code block to a prooftree LaTeX environment.
        return {
            pandoc.RawBlock("latex", "\\begin{prooftree}"),
            pandoc.RawBlock("latex", block.text),
            pandoc.RawBlock("latex", "\\end{prooftree}")
        }
    end

    -- This code block is left as is.
    return block
end
