-- Lua Filter allowing "tikz" code blocks to be interpreted in LaTeX
-- thanks to the "tikzpicture" LaTeX package.
-- Copyright (c) 2020 - Romain WALLON, released under MIT license.

function CodeBlock(block)
    if block.classes[1] == "tikz"
    then
        -- Converting this code block to a plantuml LaTeX environment.
        return {
            pandoc.RawBlock("latex", "\\begin{figure}"),
            pandoc.RawBlock("latex", "\\centering"),
            pandoc.RawBlock("latex", "\\begin{tikzpicture}"),
            pandoc.RawBlock("latex", block.text),
            pandoc.RawBlock("latex", "\\end{tikzpicture}")
            pandoc.RawBlock("latex", "\\end{figure}")
        }
    end

    -- This code block is left as is.
    return block
end
