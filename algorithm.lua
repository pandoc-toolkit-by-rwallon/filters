-- Lua Filter allowing "algorithm" code blocks to be interpreted in LaTeX
-- thanks to the "algorithm2e" LaTeX package.
-- Copyright (c) 2020 - Romain WALLON, released under MIT license.

function CodeBlock(block)
    if block.classes[1] == "algorithm"
    then
        -- Converting this code block to a plantuml LaTeX environment.
        return {
            pandoc.RawBlock("latex", "\\begin{algorithm}"),
            pandoc.RawBlock("latex", block.text),
            pandoc.RawBlock("latex", "\\end{algorithm}")
        }
    end

    -- This code block is left as is.
    return block
end
